@extends('admin_layout')
@section('admin_content')

<div class="container">

<table class="table table-Info table-hover">
 <tr>
     <th>Id</th>
     <th>Name</th>
     <th>Email</th>
     <th>Role</th>
     <th></th>
 </tr>
 @foreach ($users as $user)

 <tr>
     <td>{{ $user->id }}</td>
     <td>{{ $user->name }}</td>
     <td>{{ $user->email }}</td>
     <td>{{ $user->role }}</td>
     <td><a href="#" class="btn btn-info" role="button">Edit</a>
     <a href="{{ url('admin/delete',$user->id) }}" class="btn btn-danger" role="button">Delete</a></td>
 </tr>
 @endforeach



</table>


</div>

@endsection
