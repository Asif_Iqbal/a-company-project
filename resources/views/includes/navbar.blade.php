 <!--/ Nav Star /-->
  <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
    <img src="{{asset('frontend/img/img.png')}}" style="width:110px;height:80px;>
    <p class="intro-subtitle intro-price">
                      <a href="#"><span class="price-a" style="color:orange;">Tü-E Consortium</span></a>
                    </p>

      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
             <a class="nav-link active" href="index.html">Home</a>
          </li>

      <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
             About Us
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">About The Company</a>
              <a class="dropdown-item" href="#">Key Management</a>
               <a class="dropdown-item" href="#">Vission</a>
              <a class="dropdown-item" href="#">Mission</a>


            </div>
          </li>



          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
            Business
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">What We Do</a>
              <a class="dropdown-item" href="#">Who Are Eligble</a>
              <a class="dropdown-item" href="#">How To Apply</a>


            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
             Services
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#"> Financial Consultancy</a>
              <a class="dropdown-item" href="#"> Share  The Compliance</a>
              <a class="dropdown-item" href="#">Educational Purpose</a>

        <a class="dropdown-item" href="#">Academic Refference</a>
              <a

            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
            Process
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Check List</a>
              <a class="dropdown-item" href="#">KYC</a>
              <a class="dropdown-item" href="#">LOY</a>


            </div>
          </li>
          <li class="nav-item">

            <a class="nav-link" href="contact.html">Contact Us</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              @if(Auth::check())
              {{ Auth::user()->name }}
                @else
                User
                @endif
                </a>
                @Auth
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 {{ __('Logout') }}
             </a>
             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrfoo
             </form>

             @if (Auth::user()->role == "admin")
             <a class="dropdown-item" href="{{ route('dashboard.admin') }}"> Admin Dashbord </a>

            </div>
            @endif
            @endAuth

            @guest

            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                 <a class="dropdown-item" href="{{ route('login') }}">{{ __('login') }}</a>
                <a class="dropdown-item" href="{{ route('register') }}">{{ __('Register') }}</a>

            </div>
            @endguest


          </li>
        </ul>
      </div>
      {{-- <button type="button" class="btn btn-b-n navbar-toggle-box-collapse d-none d-md-block" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button> --}}
    </div>
  </nav>
