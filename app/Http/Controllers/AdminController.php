<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;

class AdminController extends Controller
{
    public function index()
    {

        return view('admin_login');
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function user()
    {
        $users = User::all();
        return view('admin.user.index', compact('users'));
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return redirect('admin/delete');
    }
}
